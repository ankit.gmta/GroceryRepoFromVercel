import { useState, useEffect } from 'react';
// export  const MainGlobalURL = 'http://127.0.0.1:8000';
// export const MainbaseURL = MainGlobalURL+'/api/';

export const URLS = {
  // MainGlobalURL: 'http://118.189.213.8:8053/',
  // MainbaseURL: 'http://118.189.213.8:8053/api/',

  MainGlobalURL: 'http://127.0.0.1:8000',
  MainbaseURL: 'http://127.0.0.1:8000/api/',

  MainGlobalCakeURL: 'http://118.189.213.8:8053/web_services13',
  MainCakeBaseURL: 'http://118.189.213.8:8053/web_services13/',

  // MainGlobalURL : 'https://grocery-repo-from-vercel-lbkf419u8-ankitgmta.vercel.app/',
  // MainbaseURL : 'https://grocery-repo-from-vercel-lbkf419u8-ankitgmta.vercel.app/api/',
  ImageURL: '/assets/images/',
  // Add more API URLs as needed
};

export async function ApiCall(url = '', newData = {}) {
  var token: any = localStorage.getItem('token');
  console.log('cors issue value', localStorage.getItem('cors'));
  var cors: any = localStorage.getItem('cors') == 'exists' ? 'exists' : 'false';
  console.log('cors cors value', cors);
  // var lang_id : any = localStorage.getItem('lang_id');
  // lang_id = (lang_id==null || lang_id==undefined) ? 1 : lang_id;
  // newData = { ...newData, lang_id: lang_id };
  const headers: any = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  };
  if (cors == 'false') {
    headers['Authorization'] = `Bearer ${token}`;
  }
  const response = await fetch(url, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(newData),
  });
  return response.json();
}
