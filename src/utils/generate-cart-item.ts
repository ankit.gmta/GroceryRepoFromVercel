import isEmpty from 'lodash/isEmpty';
interface Item {
  id: string | number;
  product_description: any;
  product_warehouse: any;
  product_file: any;
  product_inventories: any;
  name: string;
  slug: string;
  image: {
    thumbnail: string;
    [key: string]: unknown;
  };
  price: any;
  sale_price?: any;
  quantity?: any;
  [key: string]: unknown;
}
interface Variation {
  id: string | number;
  title: string;
  price: number;
  sale_price?: number;
  quantity: number;
  [key: string]: unknown;
}
export function generateCartItem(item: Item, variation: Variation) {
  // console.log('item generate value',item);
  // const {  quantity } = item;
  const name: any = item.product_description?.[0]?.name || 'Default Name';
  const unit: any = item.unit || 'USD';
  const price: any = item.product_warehouse?.[0]?.product_acutal_price ?? 0;
  const sale_price: any = item.product_warehouse?.[0]?.product_price ?? 0;
  const id: any = item.id || 'USD';
  const slug: any = item.id || 'USD';
  const quantity: any = item.product_inventories?.[0]?.qty ?? 0;
  const image: any = item.product_file?.[0]?.file_name || '';
  if (!isEmpty(variation)) {
    return {
      id: `${id}.${variation.id}`,
      productId: id,
      name: `${name} - ${variation.title}`,
      slug,
      unit,
      stock: variation.quantity,
      price: variation.sale_price ? variation.sale_price : variation.price,
      image: image?.thumbnail,
      variationId: variation.id,
    };
  }
  return {
    id,
    name,
    slug,
    unit,
    image: image?.thumbnail,
    stock: quantity,
    price: sale_price ? sale_price : price,
  };
}
