import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useModalAction } from '@components/common/modal/modal.context';
export default function CheckCheckoutPage() {
  const { closeModal, openModal } = useModalAction();
  const router = useRouter();
  useEffect(() => {
    if (router.pathname === '/checkout') {
      //   setShowLoginModal(true);
      openModal('LOGIN_VIEW');
    }
  }, [router.pathname, openModal]);

  return '';
}
