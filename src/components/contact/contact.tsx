'use client';
import { useContactQuery } from '@framework/contact/contact';
import ContactBox from '@components/contact/contact-content';
import { useEffect, useState } from 'react';

const ContactPage: React.FC = () => {
  let { data, isLoading } = useContactQuery();
  var localData: any;

  if (typeof window !== 'undefined') {
    const getAlternate: any = localStorage.getItem('alternate_mobile');

    const getLocal: any = localStorage.getItem('user_data');
    const ContactArray: any = [];
    if (getLocal != null && getLocal !== 'undefined') {
      const userData = JSON.parse(getLocal);
      ContactArray.push({
        default: true,
        id: userData.id,
        // "id": 1,
        mobile: userData.mobile,
        title: 'Primary Number',
      });
    }
    if (getAlternate != null && getAlternate !== 'undefined') {
      const getAlternateData = JSON.parse(getAlternate);
      ContactArray.push(getAlternateData.data[0]);
    }

    const formatData: any = { data: ContactArray };
    localData = formatData;
  }

  return !isLoading ? (
    <div className="w-full max-w-[1300px] mx-auto">
      <div className="flex flex-wrap">
        <div className="w-full">
          <ContactBox items={localData} />
        </div>
      </div>
    </div>
  ) : (
    <div>Loading...</div>
  );
};

export default ContactPage;
