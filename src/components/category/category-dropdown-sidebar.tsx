import Alert from '@components/ui/alert';
import Scrollbar from '@components/ui/scrollbar';
import SidebarMenu from '@components/ui/sidebar-menu';
import CategoryListCardLoader from '@components/ui/loaders/category-list-card-loader';
import { useCategoriesQuery } from '@framework/category/get-all-categories';
import cn from 'classnames';
import { useState, useEffect } from 'react';
import { URLS, ApiCall } from './../../global/webServiceHandler';

interface CategorySidebarProps {
  className?: string;
}

export default function CategoryDropdownSidebar({
  className,
}: CategorySidebarProps) {
  const [levelOne, setlevelOne]: any = useState([]);
  const [allCat, setallCat]: any = useState([]);
  var [loading, setloading] = useState(true);
  const APIurl = URLS.MainbaseURL;
  useEffect(() => {
    async function fetchData() {
      const data = '';
      const url = APIurl + 'post_data';
      try {
        const response = await ApiCall(url, data);
        // do something with the response
        setloading(false);
        const levelOneCat = response.data.filter((value: { level: number }) => {
          return value.level === 1;
        });
        const category = {
          ...response,
          data: levelOneCat,
        };
        setallCat(response); // set the category with level 1 in new data constant
        setlevelOne(category); // set the category with level 1 in new data constant
      } catch (error) {
        alert('something went wrong');
        // handle the error
      }
    }
    fetchData();
  }, [APIurl]);
  const { data } = useCategoriesQuery({
    limit: 10,
  });
  // console.log("newdata",levelOne);
  // console.log("data",data);
  // console.log('loading',loading);
  // console.log('allCat catdrop down',allCat);
  return (
    <aside className={cn('category-mobile-sidebar', className)}>
      <div className="max-h-full overflow-hidden border rounded border-border-base">
        {loading ? (
          Array.from({ length: 15 }).map((_, idx) => (
            <CategoryListCardLoader
              key={`category-list-${idx}`}
              uniqueKey="category-list-card-loader"
            />
          ))
        ) : (
          <SidebarMenu
            items={levelOne && levelOne.data}
            allCat={allCat && allCat.data}
          />
        )}
      </div>
    </aside>
  );
}
