import ProductsCarousel from '@components/product/products-carousel';
import { useRelatedProductsQuery } from '@framework/product/get-related-product';
import { LIMITS } from '@framework/utils/limits';
import { URLS, ApiCall } from './../../../global/webServiceHandler';
import { useState, useEffect } from 'react';

interface SimilarProductFeedProps {
  carouselBreakpoint?: {} | any;
  className?: string;
  uniqueKey?: string;
}

const SimilarProductFeed: React.FC<SimilarProductFeedProps> = ({
  carouselBreakpoint,
  className,
  uniqueKey = 'related-product-popup',
}) => {
  var [loading, setloading] = useState(true);
  const [allProd, setAllProd]: any = useState([]);
  const [allProdWithData, setAllProdWithData]: any = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const url = `${URLS.MainbaseURL}product_show_home_slider`;
      try {
        const response = await ApiCall(url);
        setloading(false);
        if (response.status == 200) {
          console.log('response', response);
          setAllProd(response.data);
          const obj: any = { pages: [{ data: response.data.data }] }; // this is done as product listing (in template) is shown in this format
          setAllProdWithData(obj);
        }
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const { data, isLoading, error } = useRelatedProductsQuery({
    limit: LIMITS.RELATED_PRODUCTS_LIMITS,
  });
  console.log('data in related products', data);
  return (
    <ProductsCarousel
      sectionHeading="text-similar-products"
      categorySlug="/search"
      className={className}
      products={allProd}
      loading={isLoading}
      error={error?.message}
      limit={LIMITS.RELATED_PRODUCTS_LIMITS}
      uniqueKey={uniqueKey}
      carouselBreakpoint={carouselBreakpoint}
    />
  );
};

export default SimilarProductFeed;
