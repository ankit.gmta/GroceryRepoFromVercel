import { Fragment, useState, useEffect } from 'react';
import ProductCardAlpine from '@components/product/product-cards/product-card-alpine';
import type { FC } from 'react';
import { useProductsQuery } from '@framework/product/get-all-products';
import ProductCardLoader from '@components/ui/loaders/product-card-loader';
import SectionHeader from '@components/common/section-header';
import { useModalAction } from '@components/common/modal/modal.context';
import slice from 'lodash/slice';
import Alert from '@components/ui/alert';
import cn from 'classnames';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { LIMITS } from '@framework/utils/limits';
import { Product } from '@framework/types';
import { URLS, ApiCall } from './../../../global/webServiceHandler';
import { toArray } from 'lodash';
interface ProductFeedProps {
  element?: any;
  className?: string;
}
var responseError: any = '';
const AllProductFeed: FC<ProductFeedProps> = ({ element, className = '' }) => {
  var [loading, setloading] = useState(true);
  const { t } = useTranslation('common');

  const { query } = useRouter();
  const {
    isFetching: isLoading,
    isFetchingNextPage: loadingMore,
    fetchNextPage,
    hasNextPage,
    data,
    error,
  } = useProductsQuery({ limit: LIMITS.PRODUCTS_LIMITS, ...query });
  const [allProd, setAllProd]: any = useState([]);
  const [allProdWithData, setAllProdWithData]: any = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const url = `${URLS.MainbaseURL}product_show_home_slider`;
      try {
        const response = await ApiCall(url);
        setloading(false);
        if (response.status == 200) {
          console.log('response', response);
          setAllProd(response.data);
          const obj: any = { pages: [{ data: response.data.data }] }; // this is done as product listing (in template) is shown in this format
          setAllProdWithData(obj);
        } else {
          responseError = response.msg;
        }
      } catch (error) {
        responseError = error;
        console.error(error);
      }
    };
    fetchData();
  }, []);
  const { openModal } = useModalAction();
  console.log('Theme Data', data);
  function handleCategoryPopup() {
    openModal('CATEGORY_VIEW');
  }
  // console.log('allProd value check',allProd.data.length);
  return (
    <div className={cn(className)}>
      <div className="flex items-center justify-between pb-0.5 mb-4 lg:mb-5 xl:mb-6">
        <SectionHeader sectionHeading="All Products" className="mb-0" />
        <div
          className="lg:hidden transition-all text-brand -mt-1.5 font-semibold text-sm md:text-15px hover:text-brand-dark"
          role="button"
          onClick={handleCategoryPopup}
        >
          {t('text-categories')}
        </div>
      </div>
      {/* //Below code is the original one  */}
      {/* {responseError ? (      
        <Alert message={responseError} />
      ) : (
        <div className="grid grid-cols-2 gap-3 sm:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 3xl:grid-cols-6 md:gap-4 2xl:gap-5">
          {isLoading && !data?.pages?.length ? (
            Array.from({ length: LIMITS.PRODUCTS_LIMITS }).map((_, idx) => (
              <ProductCardLoader
                key={`product--key-${idx}`}
                uniqueKey={`product--key-${idx}`}
              />
            ))
          ) : (
            <>
              {data?.pages?.map((page: any, index) => {
                return (
                  <Fragment key={index}>
                    {page?.data?.slice(0, 18)?.map((product: Product) => (
                      <ProductCardAlpine
                        key={`product--key${product.id}`}
                        product={product}
                      />
                    ))}
                    {element && <div className="col-span-full">{element}</div>}
                    {page?.data?.length! > 18 &&
                      slice(page?.data, 18, page?.data?.length).map(
                        (product: any) => (
                          <ProductCardAlpine
                            key={`product--key${product.id}`}
                            product={product}
                          />
                        )
                      )}
                  </Fragment>
                );
              })}
            </>
          )}
        </div>
      )} */}

      {responseError ? (
        <Alert message={responseError} />
      ) : (
        <div className="grid grid-cols-2 gap-3 sm:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 3xl:grid-cols-6 md:gap-4 2xl:gap-5">
          {loading &&
          allProd
            .map((prod: { data: string | any[] }) => prod.data.length)
            .reduce((acc: any, curr: any) => acc + curr, 0) == 0 ? (
            Array.from({ length: LIMITS.PRODUCTS_LIMITS }).map((_, idx) => (
              <ProductCardLoader
                key={`product--key-${idx}`}
                uniqueKey={`product--key-${idx}`}
              />
            ))
          ) : (
            <>
              {allProdWithData?.pages?.map((newdata: any, index: any) => {
                return (
                  <Fragment key={index}>
                    {newdata?.data?.slice(0, 18)?.map((product: Product) => {
                      return (
                        <ProductCardAlpine
                          key={`product--key${product.id}`}
                          product={product}
                        />
                      );
                    })}
                    {element && <div className="col-span-full">{element}</div>}
                    {newdata?.data?.length! > 18 &&
                      slice(newdata?.data, 18, newdata?.data?.length).map(
                        (product: any) => (
                          <ProductCardAlpine
                            key={`product--key${product.id}`}
                            product={product}
                          />
                        )
                      )}
                  </Fragment>
                );
              })}
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default AllProductFeed;
