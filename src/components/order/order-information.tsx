import { IoCheckmarkCircle } from 'react-icons/io5';
import OrderDetails from '@components/order/order-details';
import { useOrderQuery } from '@framework/order/get-order';
import { useRouter } from 'next/router';
import { URLS, ApiCall } from './../../global/webServiceHandler';
import usePrice from '@framework/product/use-price';
import { useTranslation } from 'next-i18next';
import { useEffect, useState } from 'react';

export default function OrderInformation() {
  const {
    query: { id },
  } = useRouter();
  const { t } = useTranslation('common');
  const APIurl = URLS.MainbaseURL;
  var [loading, setloading] = useState(true);
  const [orderRecord, setOrderRecord]: any = useState();
  const [scheduleDate, setScheduleDate] = useState();

  useEffect(() => {
    async function fetchData() {
      localStorage.setItem('cors', 'false');
      const url = APIurl + 'get-order-details';
      try {
        const response = await ApiCall(url);
        if (response.status == 200) {
          setloading(false);
          setOrderRecord(response.data);
        } else if (response.status == 201) {
          setloading(true);
        }
      } catch (error) {
        alert('something went wrong');
      }
    }
    fetchData();
  }, [APIurl]);
  const { data, isLoading } = useOrderQuery(id?.toString()!);
  if (typeof window !== 'undefined') {
    const getDelivery_schedule_date: any = localStorage.getItem(
      'delivery_schedule_date'
    );
    const userDelivery_schedule_date = JSON.parse(getDelivery_schedule_date);
    setScheduleDate(userDelivery_schedule_date);
  }
  const { price: total } = usePrice(
    orderRecord && {
      amount: orderRecord.shipping_fee
        ? orderRecord.total + orderRecord.shipping_fee
        : orderRecord.total,
      currencyCode: 'USD',
    }
  );
  if (loading) return <p>Loading...</p>;
  return (
    <div className="py-16 xl:px-32 2xl:px-44 3xl:px-56 lg:py-20">
      <div className="flex items-center justify-start px-4 py-4 mb-6 text-sm border rounded-md border-border-base bg-fill-secondary lg:px-5 text-brand-dark md:text-base lg:mb-8">
        <span className="flex items-center justify-center w-10 h-10 rounded-full ltr:mr-3 rtl:ml-3 lg:ltr:mr-4 lg:rtl:ml-4 bg-brand bg-opacity-20 shrink-0">
          <IoCheckmarkCircle className="w-5 h-5 text-brand" />
        </span>
        {t('text-order-received')}
      </div>

      <ul className="flex flex-col border rounded-md border-border-base bg-fill-secondary md:flex-row mb-7 lg:mb-8 xl:mb-10">
        <li className="px-4 py-4 text-base font-semibold border-b border-dashed text-brand-dark lg:text-lg md:border-b-0 md:border-r border-border-two lg:px-6 xl:px-8 md:py-5 lg:py-6 last:border-0">
          <span className="block text-xs font-normal leading-5 uppercase text-brand-muted">
            {t('text-order-number')}:
          </span>
          {orderRecord?.tracking_number}
        </li>
        <li className="px-4 py-4 text-base font-semibold border-b border-gray-300 border-dashed text-brand-dark lg:text-lg md:border-b-0 md:border-r lg:px-6 xl:px-8 md:py-5 lg:py-6 last:border-0">
          <span className="uppercase text-[11px] block text-brand-muted font-normal leading-5">
            {t('text-date')}:
          </span>
          {scheduleDate}
        </li>
        <li className="px-4 py-4 text-base font-semibold border-b border-gray-300 border-dashed text-brand-dark lg:text-lg md:border-b-0 md:border-r lg:px-6 xl:px-8 md:py-5 lg:py-6 last:border-0">
          <span className="uppercase text-[11px] block text-brand-muted font-normal leading-5">
            {t('text-email')}:
          </span>
          {orderRecord?.customer?.email}
        </li>
        <li className="px-4 py-4 text-base font-semibold border-b border-gray-300 border-dashed text-brand-dark lg:text-lg md:border-b-0 md:border-r lg:px-6 xl:px-8 md:py-5 lg:py-6 last:border-0">
          <span className="uppercase text-[11px] block text-brand-muted font-normal leading-5">
            {t('text-total')}:
          </span>
          {total}
        </li>
        <li className="px-4 py-4 text-base font-semibold border-b border-gray-300 border-dashed text-brand-dark lg:text-lg md:border-b-0 md:border-r lg:px-6 xl:px-8 md:py-5 lg:py-6 last:border-0">
          <span className="uppercase text-[11px] block text-brand-muted font-normal leading-5">
            {t('text-payment-method')}:
          </span>
          {orderRecord?.payment_gateway}
        </li>
      </ul>

      <p className="mb-8 text-sm text-brand-dark md:text-base">
        {t('text-pay-cash')}
      </p>

      <OrderDetails key={`${orderRecord.id}`} item={orderRecord} />
    </div>
  );
}
