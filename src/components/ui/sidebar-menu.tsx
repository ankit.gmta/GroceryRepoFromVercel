import { useRouter } from 'next/router';
import cn from 'classnames';
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io';
import { useUI } from '@contexts/ui.context';
import { useEffect, useState } from 'react';
import Image from '@components/ui/image';
import { useTranslation } from 'next-i18next';
import { URLS } from './../../global/webServiceHandler';
var CheckSlug = '';
var CheckId: any;
var CheckLevel = 1;
function SidebarMenuItem({
  className,
  LevelOneCat,
  item,
  allCat,
  depth = 0,
  slugName,
}: any) {
  const [levelSecondCat, setLevelSecondCat] = useState([]);
  const [levelThirdCat, setLevelThirdCat]: any = useState([]);
  const [levelsec, setlevelsec]: any = useState(false);

  const { t } = useTranslation('common');
  const router = useRouter();
  const active = router?.query?.category;
  const isActive =
    (active === item.slug && item.level === 1) ||
    item?.children?.some((_item: any) => _item.slug === active);
  const [isOpen, setOpen] = useState<boolean>(isActive);
  const [isLevelSecOpen, setLevelSecOpen] = useState<boolean>(isActive);
  const [expandLevelSecIcon, setexpandLevelSecIcon]: any = useState('');
  const [flag, setFlag]: any = useState(0);
  useEffect(() => {
    if (CheckLevel == 1) {
      setOpen(isActive);
    }
  }, [isActive]);
  const { slug, name, children: items, icon, image } = item;

  const { displaySidebar, closeSidebar } = useUI();
  function toggleCollapseLevelSec() {
    setLevelSecOpen((prevValue) => !prevValue);
  }
  function toggleCollapse() {
    if (CheckSlug == null || CheckSlug == '') {
      setOpen((prevValue) => !prevValue);
    }
  }
  function toggleCollapseThirdCat() {
    setOpen((prevValue) => !prevValue);
  }

  function onClick(position: any) {
    if (item.level === 2) {
      CheckId = item.id;
      CheckSlug = item.slug;
    }
    const myObject = allCat.find(
      (value: { id: any; level: number; slug: any }) => {
        if (value.slug === slug && value.level === 1) {
          const filteredItems = allCat.filter(
            (newValue: {
              id: any;
              parent_id: any;
              level: number;
              slug: any;
            }) => {
              if (newValue.parent_id === value.id && newValue.level === 2) {
                if (position == 1) {
                  const LevelThirdItems = allCat.filter(
                    (LevelThirdValue: {
                      parent_id: any;
                      level: number;
                      slug: any;
                    }) => {
                      if (
                        LevelThirdValue.parent_id === CheckId &&
                        LevelThirdValue.level === 3
                      ) {
                        return (
                          LevelThirdValue.parent_id === CheckId &&
                          LevelThirdValue.level === 3
                        );
                      }
                    }
                  );
                  if (
                    Array.isArray(LevelThirdItems) &&
                    LevelThirdItems.length > 0
                  ) {
                    setLevelThirdCat(LevelThirdItems); // set Level third categories in LevelThirdCat Variable
                  } else {
                    setLevelThirdCat();
                  }
                }
                return newValue.parent_id === value.id && newValue.level === 2;
              }
            }
          );
          if (Array.isArray(filteredItems) && !!filteredItems.length) {
            setLevelSecOpen(isActive);
            setlevelsec(true);
          }
          setLevelSecondCat(filteredItems);
          return true;
        }
        return false;
      }
    );

    const ExpandCollapsedWorkingLevelThirdCat = allCat.filter(
      (LevelThirdValue: { parent_id: any; level: number; slug: any }) => {
        if (
          LevelThirdValue.parent_id === CheckId &&
          LevelThirdValue.level === 3
        ) {
          return (
            LevelThirdValue.parent_id === CheckId && LevelThirdValue.level === 3
          );
        }
      }
    );

    if (
      Array.isArray(levelSecondCat) &&
      !!levelSecondCat.length &&
      ExpandCollapsedWorkingLevelThirdCat.length > 0
    ) {
      if (position == 0) {
        toggleCollapseThirdCat();
      }
    }
    if (
      Array.isArray(levelSecondCat) &&
      !!levelSecondCat.length &&
      ExpandCollapsedWorkingLevelThirdCat.length === 0
    ) {
    } else {
      const { pathname, query } = router;
      const { type, ...rest } = query;
      router.push(
        {
          pathname,
          query: { ...rest, category: slug },
        },
        undefined,
        {
          scroll: false,
        }
      );
      displaySidebar && closeSidebar();
    }
  }

  useEffect(() => {
    if (
      Array.isArray(levelSecondCat) &&
      !!levelSecondCat.length &&
      levelThirdCat.length === 0
    ) {
      alert('No more categories to show.' + levelThirdCat.length);
      toggleCollapse();
    }
  }, [levelSecondCat]);

  // level sec
  useEffect(() => {
    async function applyCondition() {
      await setFlag(1);
      if (Array.isArray(levelThirdCat) && !!levelThirdCat.length) {
        await setexpandLevelSecIcon(
          <IoIosArrowDown className="text-base text-brand-dark text-opacity-40" />
        );
      }
    }
    if (levelsec === true) {
      applyCondition();
    }
  }, [levelThirdCat, levelsec, levelThirdCat.length]);

  function setSlug() {
    onClick(1);
    CheckLevel = 2;
  }
  function mainCatFun() {
    onClick(0);
    CheckLevel = 1;
  }

  let expandIcon;
  if (Array.isArray(LevelOneCat) && LevelOneCat.length) {
    expandIcon = !isOpen ? (
      <IoIosArrowDown className="text-base text-brand-dark text-opacity-40" />
    ) : (
      <IoIosArrowUp className="text-base text-brand-dark text-opacity-40" />
    );
  }
  return (
    <>
      <li
        key={`${name}-0`}
        data-name={name}
        onClick={() => mainCatFun()}
        className={`flex justify-between items-center transition  ${
          className
            ? className
            : 'text-sm md:text-15px hover:bg-fill-base border-t border-border-base first:border-t-0 px-3.5 2xl:px-4 py-3 xl:py-3.5 2xl:py-2.5 3xl:py-3'
        } ${isOpen ? 'bg-fill-base' : 'text-brand-dark text-opacity-70'}`}
      >
        <button
          className={cn(
            'flex items-center w-full ltr:text-left rtl:text-right outline-none focus:outline-none group focus:ring-0 focus:text-brand-dark'
          )}
        >
          {image && (
            <div className="inline-flex shrink-0 2xl:w-12 2xl:h-12 3xl:w-auto 3xl:h-auto">
              <Image
                src={
                  URLS.ImageURL + 'products/p-17-m.png' ??
                  '/assets/placeholder/category-small.svg'
                }
                alt={name || t('text-category-thumbnail')}
                width={40}
                height={40}
                style={{ width: 'auto' }}
              />
            </div>
          )}
          <span className="text-brand-dark group-hover:text-opacity-80 capitalize ltr:pl-2.5 rtl:pr-2.5 md:ltr:pl-4 md:rtl:pr-4 2xl:ltr:pl-3 2xl:rtl:pr-3 3xl:ltr:pl-4 3xl:rtl:pr-4">
            {name}
          </span>
          {expandIcon && (
            <span className="ltr:ml-auto rtl:mr-auto">{expandIcon}</span>
          )}
        </button>
      </li>
      {Array.isArray(levelSecondCat) && levelSecondCat.length > 0 && isOpen ? (
        <li
          key={`uniqueIdLevel2-1`}
          data-id="uniqueIdLevel2"
          onClick={() => setSlug()}
          className={`flex justify-between items-center transition ${
            className
              ? className
              : 'text-sm md:text-15px hover:bg-fill-base border-t border-border-base first:border-t-0 px-3.5 2xl:px-4 py-3 xl:py-3.5 2xl:py-2.5 3xl:py-3'
          } ${isOpen ? 'bg-fill-base' : 'text-brand-dark text-opacity-70'}`}
        >
          <ul key="content" className="py-3 text-xs border-border-base">
            {levelSecondCat?.map((currentItem: any) => {
              const childDepth = depth + 1;
              return (
                <>
                  <SidebarMenuItem
                    key={`${currentItem.name}${currentItem.slug}`}
                    item={currentItem}
                    allCat={allCat}
                    depth={childDepth}
                    slugName={currentItem.slug}
                    className={cn(
                      'text-sm ltr:pl-14 rtl:pr-14 py-2.5 ltr:pr-4 rtl:pl-4'
                    )}
                  />
                  {/* { Array.isArray(levelThirdCat) && levelThirdCat.length > 0 ? (<ul><li>Check UI</li><li>Check UI</li><li>Check UI</li><li>Check UI</li></ul>) : ('') } */}
                  {/* {flag > 0 && expandIcon !== undefined && <span className="new">{expandLevelSecIcon}</span>} */}
                  {Array.isArray(levelThirdCat) &&
                  levelThirdCat.length > 0 &&
                  currentItem.slug === CheckSlug
                    ? LevelThirdCatDiv(levelThirdCat)
                    : ''}
                </>
              );
            })}
          </ul>
        </li>
      ) : null}
      {/* {Array.isArray(levelThirdCat) && levelThirdCat.length > 0 ? (
        <li key="uniqueIdLevel3">
          <ul
            key="content"
            className="py-3 text-xs border-border-base"
          >
            {levelThirdCat?.map((currentItem : any) => {
              const childDepth = depth + 1;
              return (
                <SidebarMenuItem
                  key={`${currentItem.name}${currentItem.slug}`}            
                  item={currentItem}                  
                  depth={childDepth}
                  className={cn(
                    'text-sm ltr:pl-14 rtl:pr-14 py-2.5 ltr:pr-4 rtl:pl-4'
                  )}                  
                />  
              );
                
            })}
          </ul>
        </li>
      ) : null} */}
    </>
  );
}
function LevelThirdCatDiv(levelThirdCat: any[]) {
  return (
    <>
      {Array.isArray(levelThirdCat) && levelThirdCat.length > 0 ? (
        <li
          className="flex justify-between items-center transition text-sm md:text-15px hover:bg-fill-base border-t border-border-base first:border-t-0 px-3.5 2xl:px-4 py-3 xl:py-3.5 2xl:py-2.5 3xl:py-3 text-brand-dark text-opacity-70 ml-10"
          key="uniqueIdLevel3"
        >
          <ul key="content" className="py-3 text-xs border-border-base">
            {levelThirdCat?.map((currentItem: any) => {
              return (
                <SidebarMenuItem
                  key={`${currentItem.name}${currentItem.slug}`}
                  item={currentItem}
                  slugName={currentItem.slug}
                  className={cn(
                    'text-sm ltr:pl-14 rtl:pr-14 py-2.5 ltr:pr-4 rtl:pl-4'
                  )}
                />
              );
            })}
          </ul>
        </li>
      ) : null}
    </>
  );
}
function SidebarMenu({ items, allCat, className }: any) {
  return (
    <ul className={cn(className)}>
      {items?.map((item: any) => (
        <SidebarMenuItem
          key={`${item.slug}-key-${item.id}`}
          LevelOneCat={items}
          item={item}
          allCat={allCat}
          slugName={item.slug}
        />
      ))}
    </ul>
  );
}

export default SidebarMenu;
