import BannerCard from '@components/cards/banner-card';
import Carousel from '@components/ui/carousel/carousel';
import { Swiper, SwiperSlide } from 'swiper/react';
import { URLS, ApiCall } from './../../global/webServiceHandler';
import { useState, useEffect } from 'react';
const breakpoints = {
  '1536': {
    slidesPerView: 3,
    spaceBetween: 20,
  },
  '1280': {
    slidesPerView: 3,
    spaceBetween: 16,
  },
  '1024': {
    slidesPerView: 3,
    spaceBetween: 16,
  },
  '768': {
    slidesPerView: 2,
    spaceBetween: 16,
  },
  '520': {
    slidesPerView: 2,
    spaceBetween: 12,
  },
  '0': {
    slidesPerView: 1,
  },
};

interface BannerProps {
  data: any;
  className?: string;
  buttonSize?: 'default' | 'small';
}

const BannerAllCarousel: React.FC<BannerProps> = ({
  data,
  className = 'mb-6',
  buttonSize = 'default',
}) => {
  const [allCat, setAllCat] = useState([]);
  const modifiedCat = [...allCat, ...allCat];
  useEffect(() => {
    const fetchData = async () => {
      const url = `${URLS.MainbaseURL}category_show_home_slider`;
      try {
        const response = await ApiCall(url);
        setAllCat(response.data);
      } catch (error) {
      }
    };
    fetchData();
  }, []);
  return (
    <div className={className}>
      <Carousel
        autoplay={true}
        breakpoints={breakpoints}
        buttonSize={buttonSize}
        prevActivateId="all-banner-carousel-button-prev"
        nextActivateId="all-banner-carousel-button-next"
      >
        <Swiper loop={true}>
          {modifiedCat?.map((banner: any) => (
            <SwiperSlide key={`all-banner--key${banner.id}`}>
              <BannerCard banner={banner} effectActive={true} />
            </SwiperSlide>
          ))}
        </Swiper>
      </Carousel>
    </div>
  );
};

export default BannerAllCarousel;
function setloading(arg0: boolean) {
  throw new Error('Function not implemented.');
}
