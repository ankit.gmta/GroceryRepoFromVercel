import Button from '@components/ui/button';
import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { useState } from 'react';

const stripePromise = loadStripe(
  process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY as string
);
type Props = {
  buttonText?: string;
  getToken?: any;
};
const StripeForm: React.FC<Props> = ({ buttonText, getToken }) => {
  // Get a reference to Stripe or Elements using hooks.
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async () => {
    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }
    // Use elements.getElement to get a reference to the mounted Element.
    const cardElement: any = elements.getElement(CardElement);

    // Pass the Element directly to other Stripe.js methods:
    // e.g. createToken - https://stripe.com/docs/js/tokens_sources/create_token?type=cardElement
    const { token } = await stripe.createToken(cardElement);
    getToken(token);
    if (token) {
      console.log(token, 'token');
    }
  };
  const [activeTab, setActiveTab] = useState('cardInfo');

  const handleTabChange = (tab: any) => {
    setActiveTab(tab);
  };
  const renderTabContent = () => {
    if (typeof window !== 'undefined') {
      if (activeTab === 'cardInfo') {
        localStorage.setItem('payment_method', JSON.stringify(1)); // payment method list : 1=> pay by card, 2=> cash on delivery, 3=> pay by points
        return (
          <>
            <h3 className="text-brand-dark opacity-60 mb-3">Enter card info</h3>
            <CardElement />
          </>
        );
      } else if (activeTab === 'cashOnDelivery') {
        localStorage.setItem('payment_method', JSON.stringify(2)); // payment method list : 1=> pay by card, 2=> cash on delivery, 3=> pay by points
        // return <h3 className="text-brand-dark opacity-60 mb-3">Cash on Delivery</h3>;
        return '';
      } else if (activeTab === 'redeemPoints') {
        localStorage.setItem('payment_method', JSON.stringify(3)); // payment method list : 1=> pay by card, 2=> cash on delivery, 3=> pay by points
        // return <h3 className="text-brand-dark opacity-60 mb-3">Redeem Points</h3>;
        return '';
      }
    }
  };
  return (
    <div className="w-full bg-white rounded-xl xl:w-[500px]">
      <div className="flex">
        <Button
          className={`tab-button ${
            activeTab === 'cardInfo' ? 'active' : ''
          } h-11 md:h-12 mt-5 mr-10 mb-5`}
          type="button"
          onClick={() => handleTabChange('cardInfo')}
          variant="formButton"
        >
          Pay by Card
        </Button>
        <Button
          className={`tab-button ${
            activeTab === 'cashOnDelivery' ? 'active' : ''
          } h-11 md:h-12 mt-5 mr-10`}
          type="button"
          onClick={() => handleTabChange('cashOnDelivery')}
          variant="formButton"
        >
          Cash on Delivery
        </Button>
        <Button
          className={`tab-button ${
            activeTab === 'redeemPoints' ? 'active' : ''
          } h-11 md:h-12 mt-5`}
          type="button"
          onClick={() => handleTabChange('redeemPoints')}
          variant="formButton"
        >
          Pay by Points
        </Button>
      </div>
      {/* <hr className="hr-text" data-content="OR"/> */}
      <div className="tab-content">{renderTabContent()}</div>
      {activeTab === 'cardInfo' ? (
        <Button
          className="h-11 md:h-12 mt-5"
          type="button"
          onClick={handleSubmit}
          variant="formButton"
        >
          Pay Now
        </Button>
      ) : (
        ''
      )}
    </div>
  );
};

type Item = {
  item: {
    price: any;
    buttonText: string;
  };
};

const StripePaymentForm = ({ item: { price, buttonText } }: Item) => {
  const sendTokenToServer = async (token: any) => {};

  return (
    <Elements stripe={stripePromise}>
      <StripeForm
        getToken={(token: any) => sendTokenToServer(token)}
        buttonText={buttonText}
      />
    </Elements>
  );
};

export default StripePaymentForm;
