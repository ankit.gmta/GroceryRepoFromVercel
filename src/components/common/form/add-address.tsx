import Input from '@components/ui/form/input';
import Button from '@components/ui/button';
import TextArea from '@components/ui/form/text-area';
import { useForm } from 'react-hook-form';
import { useModalState } from '@components/common/modal/modal.context';
import { useModalAction } from '@components/common/modal/modal.context';
import CloseButton from '@components/ui/close-button';
import Heading from '@components/ui/heading';
import Map from '@components/ui/map';
import { useState, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { URLS, ApiCall } from './../../../global/webServiceHandler';
import Router from 'next/router';
import AddressPage from '@components/checkout/address';
import CheckoutPage from 'src/pages/checkout';

interface ContactFormValues {
  title: string;
  default: boolean;
  lat: number;
  lng: number;
  formatted_address?: string;
}

const AddAddressForm: React.FC = () => {
  const { t } = useTranslation();
  const { data } = useModalState();
  const [customError, setCustomError]: any = useState('');
  const [titleError, setTitleError]: any = useState('');
  const [addressError, setAddressError]: any = useState('');
  // const [refreshKey, setRefreshKey] = useState('false');

  const APIurl = URLS.MainbaseURL;
  const { closeModal } = useModalAction();
  const [signinData, setSigninData]: any = useState([]);
  function onSubmit(values: ContactFormValues, e: any) {
    const apiRequest = [
      {
        id: e.target.id.value,
        title: values.title,
        description: values.formatted_address,
      },
    ];
    if (values.title != '' && values.formatted_address != '') {
      setSigninData(apiRequest);
    }
  }
  useEffect(() => {
    async function fetchData() {
      const url = APIurl + 'save-user-address';
      try {
        if (signinData.length > 0) {
          localStorage.setItem('cors', 'false');
          const response = await ApiCall(url, signinData?.[0]);
          if (response.status == 400) {
            'title' in response.msg
              ? setTitleError(response.msg.title?.[0])
              : '';
            'description' in response.msg
              ? setAddressError(response.msg.description?.[0])
              : '';
          } else if (response.status == 401) {
            setCustomError(response.msg);
          } else {
            if (response.status == 200) {
              const jsonData = JSON.stringify(response.data);
              localStorage.setItem('user_address', jsonData);
              // setRefreshKey('true');
              Router.push('/checkout');
              closeModal();
            }
          }
        }
      } catch (error) {
        setCustomError(error);
      }
    }
    if (signinData.length > 0) {
      fetchData();
    }
  }, [signinData, APIurl, closeModal]);

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<ContactFormValues>({
    defaultValues: {
      title: data || data?.address_reference ? data?.address_reference : '',
      default: data || data?.address_reference ? data?.address_reference : '',
      formatted_address: data || data?.street ? data?.street : '',
    },
  });
  // return refreshKey=='true' ? (
  //       <CheckoutPage/>
  //     ) : (
  //       <div className="w-full md:w-[600px] lg:w-[900px] xl:w-[1000px] mx-auto p-5 sm:p-8 bg-brand-light rounded-md">
  //           <CloseButton onClick={closeModal} />
  //           <Heading variant="title" className="mb-8 -mt-1.5">
  //             {t('common:text-add-delivery-address')}
  //           </Heading>
  //           <form onSubmit={handleSubmit(onSubmit)} noValidate>
  //           <input type="hidden" name="id" value={data?.id} />
  //             <div className="mb-6">
  //               <Input
  //                 variant="solid"
  //                 label="Address Title"
  //                 {...register('title', { required: 'Title Required' })}
  //                 error={errors.title?.message}
  //               />
  //             </div>
  //             <div className="grid grid-cols-1 mb-6 gap-7">
  //               <Map
  //                 lat={data?.address?.lat || 1.295831}
  //                 lng={data?.address?.lng || 103.76261}
  //                 height={'420px'}
  //                 zoom={15}
  //                 showInfoWindow={false}
  //                 mapCurrentPosition={(value: string) =>
  //                   setValue('formatted_address', value)
  //                 }
  //               />
  //               <TextArea
  //                 label="Address"
  //                 {...register('formatted_address', {
  //                   required: 'forms:address-required',
  //                 })}
  //                 error={errors.formatted_address?.message}
  //                 className="text-brand-dark"
  //                 variant="solid"
  //               />
  //             </div>
  //             <div className="flex justify-end w-full">
  //               <Button className="h-11 md:h-12 mt-1.5" type="submit">
  //                 {t('common:text-save-address')}
  //               </Button>
  //             </div>
  //           </form>
  //         </div>
  //     );
  return (
    <div className="w-full md:w-[600px] lg:w-[900px] xl:w-[1000px] mx-auto p-5 sm:p-8 bg-brand-light rounded-md">
      <CloseButton onClick={closeModal} />
      <Heading variant="title" className="mb-8 -mt-1.5">
        {t('common:text-add-delivery-address')}
      </Heading>
      <form onSubmit={handleSubmit(onSubmit)} noValidate>
        <input type="hidden" name="id" value={data?.id} />
        <div className="mb-6">
          <Input
            variant="solid"
            label="Address Title"
            {...register('title', { required: 'Title Required' })}
            error={errors.title?.message}
          />
        </div>
        <div className="grid grid-cols-1 mb-6 gap-7">
          <Map
            lat={data?.address?.lat || 1.295831}
            lng={data?.address?.lng || 103.76261}
            height={'420px'}
            zoom={15}
            showInfoWindow={false}
            mapCurrentPosition={(value: string) =>
              setValue('formatted_address', value)
            }
          />
          <TextArea
            label="Address"
            {...register('formatted_address', {
              required: 'forms:address-required',
            })}
            error={errors.formatted_address?.message}
            className="text-brand-dark"
            variant="solid"
          />
        </div>
        <div className="flex justify-end w-full">
          <Button className="h-11 md:h-12 mt-1.5" type="submit">
            {t('common:text-save-address')}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default AddAddressForm;
