import { useAddressQuery } from '@framework/address/address';
import AddressGrid from '@components/address/address-grid';
import { URLS, ApiCall } from './../../global/webServiceHandler';
import { useState, useEffect } from 'react';
const AddressPage: React.FC = () => {
  var [loading, setloading] = useState(true);
  const [addressData, setAddressData]: any = useState([]);
  const APIurl = URLS.MainbaseURL;
  useEffect(() => {
    async function fetchData() {
      const apiRequest = '';
      const url = APIurl + 'get-user-address';
      try {
        localStorage.setItem('cors', 'false');
        const response = await ApiCall(url, apiRequest);
        if (response.status == 200) {
          setloading(false);
          setAddressData(response);
        } else if (response.status == 201) {
          setloading(false);
        }
      } catch (error) {
        alert('something went wrong');
      }
    }
    fetchData();
  }, [APIurl]);
  let { data, isLoading } = useAddressQuery();
  console.log('Address Data', data);
  return !loading ? (
    <AddressGrid addressData={addressData?.data} />
  ) : (
    <div>Loading...</div>
  );
};

export default AddressPage;
