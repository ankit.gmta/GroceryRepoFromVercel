import Link from 'next/link';
import { URLS, ApiCall } from './../../global/webServiceHandler';
import usePrice from '@framework/product/use-price';
import { useCart } from '@contexts/cart/cart.context';
import Text from '@components/ui/text';
import Button from '@components/ui/button';
import { CheckoutItem } from '@components/checkout/checkout-card-item';
import { CheckoutCardFooterItem } from './checkout-card-footer-item';
import { useTranslation } from 'next-i18next';
import Router from 'next/router';
import { ROUTES } from '@utils/routes';
import { useEffect, useRef, useState } from 'react';

const CheckoutCard: React.FC = () => {
  const APIurl = URLS.MainbaseURL;
  const [orderData, setOrderData]: any = useState([]);
  const { t } = useTranslation('common');
  const { items, total, isEmpty } = useCart();
  // const [orderDataError, setOrderDataError] : any = useState('');
  var orderDataError = useRef<any>();
  const { price: subtotal } = usePrice({
    amount: total,
    currencyCode: 'USD',
  });
  function orderHeader(items: any) {
    if (typeof window !== 'undefined') {
      const getUserAddress: any = localStorage.getItem('selected_address');
      const userData = JSON.parse(getUserAddress);

      const getUserSelfData: any = localStorage.getItem('user_data');
      const userSelfData = JSON.parse(getUserSelfData);

      const getDelivery_tip: any = localStorage.getItem('delivery_tip');
      const userDelivery_tip = JSON.parse(getDelivery_tip);

      const getPayment_method: any = localStorage.getItem('payment_method');
      const userPayment_method = JSON.parse(getPayment_method);

      const getDelivery_date_time_slot_id: any = localStorage.getItem(
        'delivery_date_time_slot_id'
      );
      const userDelivery_date_time_slot_id = JSON.parse(
        getDelivery_date_time_slot_id
      );


      const getDelivery_schedule_date: any = localStorage.getItem(
        'delivery_schedule_date'
      );
      const userDelivery_schedule_date = JSON.parse(getDelivery_schedule_date);
      const getDoorStep: any = localStorage.getItem('doorstep');
      const userDoorStep = JSON.parse(getDoorStep);
      const getAlternate: any = localStorage.getItem('alternate_mobile');
      const getAlternateData = JSON.parse(getAlternate);

      const data = {
        delivery_time_slot_id: userDelivery_date_time_slot_id,
        payment_by_points: '0',
        delivery_type: '1',
        delivery_phone: getAlternateData.mobile,
        payment_by_coupons: '1',
        card_id: '1',
        doorstep: userDoorStep,
        delivery_date: userDelivery_schedule_date,
        payment_type: userPayment_method,
        instruction: '1',
        delivery_country_code: userSelfData.country_code,
        warehouse_id: userData.warehouse_id,
        shipping_address_id: userData.id, // not confirm
        payment_status: userPayment_method,
        status: '1',
        price: '1',
        lang_id: '1',

        total_item: items.length,
        delivery_charges: 10.0,
        total_savings: 0.0,
        total_amount: total,
        subtotal: total,
        payment_by_credits: 0.0,
        payment_by_user: total,
        tax: 0.0,

        orderData: items,
      };

      if (items.length > 0) {
        setOrderData(data);
      }
    }
  }
  useEffect(() => {
    async function fetchData() {
      localStorage.setItem('cors', 'false');
      const url = APIurl + 'order-now';
      try {
        if (orderData?.orderData?.length > 0) {
          const response = await ApiCall(url, orderData);
          if (response.status == 400) {
            // ('orderData' in response.msg) ? setOrderDataError(response.msg.orderData?.[0]) : '';
            orderDataError.current = response.msg.orderData?.[0];
            // ('password' in response.msg) ? setPasswordError(response.msg.password?.[0]) : '';
          } else if (response.status == 401 || response.status == 201) {
            orderDataError.current = response.msg;
          } else {
            if (response.status == 200) {
              !isEmpty && Router.push(ROUTES.ORDER);
              // closeModal();
              // Router.push('/');
            }
          }
        }
      } catch (error) {
        orderDataError.current = error;
        // setCustomError(error);
      }
    }

    if (orderData?.orderData?.length > 0) {
      fetchData();
    }
  }, [orderData, APIurl, isEmpty]);

  const checkoutFooter = [
    {
      id: 1,
      name: t('text-sub-total'),
      price: subtotal,
    },
    {
      id: 2,
      name: t('text-shipping'),
      price: '$0',
    },
    {
      id: 3,
      name: t('text-total'),
      price: subtotal,
    },
  ];
  return (
    <>
      <div className="px-4 py-1 border rounded-md border-border-base text-brand-light xl:py-6 xl:px-7">
        <div className="flex pb-2 text-sm font-semibold rounded-md text-heading">
          <span className="font-medium text-15px text-brand-dark">
            {t('text-product')}
          </span>
          <span className="font-medium ltr:ml-auto rtl:mr-auto shrink-0 text-15px text-brand-dark">
            {t('text-sub-total')}
          </span>
        </div>
        {!isEmpty ? (
          items.map((item) => <CheckoutItem item={item} key={item.id} />)
        ) : (
          <p className="py-4 text-brand-danger text-opacity-70">
            {t('text-empty-cart')}
          </p>
        )}
        {checkoutFooter.map((item: any) => (
          <CheckoutCardFooterItem item={item} key={item.id} />
        ))}
        {orderDataError.current ? (
          <span className="required">{orderDataError.current}</span>
        ) : (
          ''
        )}
        <Button
          variant="formButton"
          className={`w-full mt-8 mb-5 bg-brand text-brand-light rounded font-semibold px-4 py-3 transition-all ${
            isEmpty && 'opacity-40 cursor-not-allowed'
          }`}
          // onClick={orderHeader}
          onClick={() => orderHeader(items)}
        >
          {t('button-order-now')}
        </Button>
      </div>
      <Text className="mt-8">
        {t('text-by-placing-your-order')}{' '}
        <Link href={ROUTES.TERMS} legacyBehavior>
          <a className="font-medium underline text-brand">
            {t('text-terms-of-service')}{' '}
          </a>
        </Link>
        {t('text-and')}{' '}
        <Link href={ROUTES.PRIVACY} legacyBehavior>
          <a className="font-medium underline text-brand">
            {t('text-privacy')}
          </a>
        </Link>
        . {t('text-credit-debit')}
      </Text>
      <Text className="mt-4">{t('text-bag-fee')}</Text>
    </>
  );
};

export default CheckoutCard;
