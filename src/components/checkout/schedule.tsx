import { Key, useEffect, useState } from 'react';
import { RadioGroup } from '@headlessui/react';
import cn from 'classnames';
import Layout from '@components/layout/layout';
import { useTranslation } from 'next-i18next';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { URLS, ApiCall } from './../../../src/global/webServiceHandler';

// const deliveryDateSchedule = [
//   'Sat, Jul 03, 2021',
//   'Sun, Jul 04, 2021',
//   'Mon, Jul 05, 2021',
//   'Tus, Jul 06, 2021',
//   'Wed, Jul 07 ,2021 ',
// ];
const currentDate = new Date(); // Get the current date
// Helper function to format the date as 'Day, Month DD, YYYY'
function formatDate(date: any) {
  const options = {
    weekday: 'short',
    month: 'short',
    day: 'numeric',
    year: 'numeric',
  };
  return date.toLocaleDateString('en-US', options);
}
function apiFormatDate(date: any) {
  const inputDate = new Date(date);
  const timezoneOffset = inputDate.getTimezoneOffset() * 60000; // Get the timezone offset in milliseconds
  const adjustedDate = new Date(inputDate.getTime() - timezoneOffset); // Adjust the date using the offset
  const apiformattedDate = adjustedDate.toISOString().slice(0, 10) + ' 00:00';
  return apiformattedDate;
}
const deliveryDateSchedule: any = [];
for (let i = 0; i < 5; i++) {
  const date = new Date(currentDate.getTime() + i * 24 * 60 * 60 * 1000); // Add i days to the current date
  const formattedDate = formatDate(date); // Format the date as 'Day, Month DD, YYYY'
  deliveryDateSchedule.push(formattedDate);
}

const APIurl = URLS.MainCakeBaseURL;
// const deliveryTimeSchedule = ['9am to 10am', '3pm to 5pm', '6pm to 8pm'];
var deliveryTimeSchedule: any = [];

export default function Schedule() {
  const [scheduleTime, setScheduleTime]: any = useState([]);
  const [timeScheduleData, setTimeScheduleData]: any = useState([]);
  const { t } = useTranslation('common');
  const [dateSchedule, setDateSchedule] = useState(deliveryDateSchedule[0]);
  function getDay(date: string) {
    const day = date.split(',');
    return day[0];
  }
  useEffect(() => {
    handleRadioOptionClick(deliveryDateSchedule[0], 'static_date');
  }, []);
  // console.log('deliveryDateSchedule deliveryDateSchedule deliveryDateSchedule',deliveryDateSchedule[0]);
  // const [timeSchedule, setTimeSchedule] = useState([]);

  function handleRadioOptionClick(date: any, date_string: any) {
    const apiformattedDate = apiFormatDate(date); // Format the date as 'Day, Month DD, YYYY';
    if (date_string == 'static_date') {
      setSelectedDate(null);
    }
    if (typeof window !== 'undefined') {
      localStorage.setItem(
        'delivery_schedule_date',
        JSON.stringify(apiformattedDate)
      );
    }
    const getSelectedAddress: any = localStorage.getItem('selected_address');
    if (getSelectedAddress != null && getSelectedAddress !== 'undefined') {
      const userData = JSON.parse(getSelectedAddress);
      const data = [
        {
          date: apiformattedDate,
          max_shipment_hours: 0,
          lang_id: userData.lang_id,
          area_id: userData.area_id,
          warehouse_id: userData.warehouse_id,
        },
      ];
      if (date != '') {
        setScheduleTime(data);
      }
    }
  }
  useEffect(() => {
    async function fetchData() {
      const url = APIurl + 'get_delivery_time_slot';
      const array: any = [];
      try {
        if (scheduleTime.length > 0) {
          localStorage.setItem('cors', 'exists');
          const response = await ApiCall(url, scheduleTime?.[0]);
          if (response.response.status == 400) {
            // ('email' in response.msg) ? setEmailError(response.msg.email?.[0]) : '';
            // ('password' in response.msg) ? setPasswordError(response.msg.password?.[0]) : '';
          } else if (response.response.status == 401) {
            // setCustomError(response.msg);
          } else {
            if (response.response.status_code == 200) {
              deliveryTimeSchedule = response.response.data.map(
                (value: any) => {
                  array.push({
                    id: value.id,
                    default: value.order_limit,
                    start_time: value.start_time,
                    end_time: value.end_time,
                  });
                  return value.start_time + ' - ' + value.end_time;
                }
              );
              // setTimeScheduleData(deliveryTimeSchedule);
              setTimeScheduleData(array);
            }
          }
        }
      } catch (error) {
        // setCustomError(error);
      }
    }
    if (scheduleTime.length > 0) {
      fetchData();
    }
  }, [scheduleTime]);

  const [timeSchedule, setTimeSchedule] = useState(deliveryTimeSchedule[0]);
  if (typeof window !== 'undefined') {
    localStorage.setItem(
      'delivery_schedule_time',
      JSON.stringify(deliveryTimeSchedule[0])
    );
  }
  useEffect(() => {
    if (deliveryTimeSchedule.length > 0) {
      if (typeof window !== 'undefined') {
        localStorage.setItem(
          'delivery_schedule_time',
          JSON.stringify(deliveryTimeSchedule[0])
        );
      }
      setTimeSchedule(deliveryTimeSchedule[0]);
    }
  }, []);

  function getMonth(date: string) {
    const month = date.split(',');
    return month[1];
  }
  const [selectedDate, setSelectedDate] = useState(null);
  // function onSelectTime(value : any)
  // {
  //   alert('selected radio btin'+value);
  //   if(typeof window !== 'undefined') {
  //     localStorage.setItem('delivery_date_time_slot_id',JSON.stringify(value));
  //   }
  // }
  const onSelectTime = (value: any) => {
    if (typeof window !== 'undefined') {
      localStorage.setItem('delivery_date_time_slot_id', JSON.stringify(value));
    }
  };
  const handleDateChange = (date: any) => {
    setSelectedDate(date);
    handleRadioOptionClick(date, 'manual_date');
  };
  let firstItemIndex = 0;
  const startDate = new Date();
  startDate.setDate(startDate.getDate() + 5); // Set the start date 5 days from the current date
  return (
    <div className="w-full">
      <div className="w-full mx-auto">
        <RadioGroup value={dateSchedule} onChange={setDateSchedule}>
          <RadioGroup.Label className="sr-only">
            {t('text-delivery-schedule')}
          </RadioGroup.Label>
          <div className="grid grid-cols-2 gap-4 sm:grid-cols-4 lg:grid-cols-6">
            {deliveryDateSchedule.map((date: any) => (
              <RadioGroup.Option
                key={date}
                value={date}
                className={({ checked }) =>
                  cn(
                    'relative rounded-lg px-5 py-3 cursor-pointer focus:outline-none',
                    checked
                      ? selectedDate == '' || selectedDate == null
                        ? 'bg-brand text-brand-light'
                        : 'bg-gray-100'
                      : 'bg-gray-100'
                  )
                }
                onClick={() => handleRadioOptionClick(date, 'static_date')}
              >
                {({ checked }) => (
                  <div className="text-center">
                    <RadioGroup.Label
                      as="p"
                      className={`text-base font-semibold  ${
                        checked
                          ? selectedDate == '' || selectedDate == null
                            ? 'text-brand-light'
                            : 'text-gray-900'
                          : 'text-gray-900'
                      }`}
                    >
                      {getDay(date)}
                    </RadioGroup.Label>
                    <RadioGroup.Description
                      as="span"
                      className={`text-15px ${
                        checked
                          ? selectedDate == '' || selectedDate == null
                            ? 'text-brand-light'
                            : 'text-gray-500'
                          : 'text-gray-500'
                      }`}
                    >
                      {getMonth(date)}
                    </RadioGroup.Description>
                  </div>
                )}
              </RadioGroup.Option>
            ))}
          </div>
        </RadioGroup>
        {/* End of date schedule */}
        <hr className="hr-text" data-content="OR" />
        <DatePicker
          selected={selectedDate}
          onChange={handleDateChange}
          minDate={startDate}
          showPopperArrow={false}
          placeholderText="Select date"
          className={`relative rounded-lg px-5 py-3 cursor-pointer focus:outline-none custom-datepicker-input ${
            selectedDate ? 'make-selected' : ''
          }`}
        />
        <RadioGroup
          className="mt-10"
          value={timeSchedule}
          onChange={setTimeSchedule}
        >
          <RadioGroup.Label className="sr-only">
            {t('text-delivery-schedule')}
          </RadioGroup.Label>
          <div className="flex flex-wrap justify-between grid-cols-2 gap-4 lg:grid sm:grid-cols-3 lg:grid-cols-4">
            {timeScheduleData.map((time: any, index: number) => (
              <RadioGroup.Option
                key={time.start_time + '-' + time.end_time}
                value={time.start_time + '-' + time.end_time}
                className="cursor-pointer focus:outline-none"
                disabled={!time.default} // Set disabled based on time.default value
                defaultChecked={index === 0}
                onClick={() => onSelectTime(time.id)}
              >
                {({ checked }) => (
                  <div className="flex items-center">
                    <span
                      className={cn(
                        'flex w-6 h-6 rounded-full',
                        checked
                          ? 'border-[6px] border-brand'
                          : 'border-2 border-gray-200'
                      )}
                    />
                    <RadioGroup.Label
                      as="p"
                      className="text-sm text-black ltr:ml-2 rtl:mr-2"
                    >
                      {time.start_time + '-' + time.end_time}
                    </RadioGroup.Label>
                  </div>
                )}
              </RadioGroup.Option>
            ))}
          </div>
        </RadioGroup>
        {/* End of time schedule */}
      </div>
    </div>
  );
}

Schedule.Layout = Layout;
