import Link from '@components/ui/link';

interface Props {
  href: string;
  btnProps: React.ButtonHTMLAttributes<any>;
  isAuthorized: boolean;
}

export default function AuthMenu({
  isAuthorized,
  href,
  btnProps,
  children,
}: React.PropsWithChildren<Props>) {
  const getLocal: any = localStorage.getItem('user_data');
  const userData = JSON.parse(getLocal);
  const isLoggedIn = userData !== null && userData !== undefined;
  return isLoggedIn ? (
    <Link
      href={href}
      className="text-sm font-normal lg:text-15px text-brand-dark focus:outline-none ltr:ml-2 rtl:mr-2"
    >
      {children} {userData.first_name}
    </Link>
  ) : (
    <button
      className="text-sm font-normal lg:text-15px text-brand-dark focus:outline-none ltr:ml-2 rtl:mr-2"
      aria-label="Authentication"
      {...btnProps}
    />
  );
}
