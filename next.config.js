// /**
//  * @type {import('next').NextConfig}
//  */
// const { i18n } = require('./next-i18next.config');
// const runtimeCaching = require('next-pwa/cache');
// const withPWA = require('next-pwa')({
//   dest: 'public',
//   disable: process.env.NODE_ENV === 'development',
//   runtimeCaching,
// });
// const nextConfig = withPWA({
//   reactStrictMode: true,
//   i18n,
//   ...(process.env.NODE_ENV === 'production' && {
//     typescript: {
//       ignoreBuildErrors: true,
//     },
//     eslint: {
//       ignoreDuringBuilds: true,
//     },
//   }),
// });

// module.exports = nextConfig;

/**
 * @type {import('next').NextConfig}
 */
const { i18n } = require('./next-i18next.config');
const nextConfig = {
  output: 'export',
  distDir: 'out',
}
const runtimeCaching = require('next-pwa/cache');
const withPWA = require('next-pwa')({
  dest: 'public',
  disable: process.env.NODE_ENV === 'development',
  runtimeCaching,
});
module.exports = withPWA({
  reactStrictMode: true,
  i18n,
  // exportPathMap: async function (defaultPathMap) {
  //   return {
  //     '/': { page: '/' },
  //     '/404': { page: '/404' },
  //     ...defaultPathMap,
  //   };
  // },
  ...(process.env.NODE_ENV === 'production' && {
    typescript: {
      ignoreBuildErrors: true,
    },
    eslint: {
      ignoreDuringBuilds: true,
    },
  }),
}), nextConfig;